<?php

namespace App\Services\FormCreator\Interfaces;

interface RenderableInterface
{
    public function render(): string;
}