<?php
/**
 * Created by PhpStorm.
 * User: daddits
 * Date: 09.11.18
 * Time: 21:31
 */

namespace App\Services\FormCreator\Elements;

use App\Services\FormCreator\Interfaces\RenderableInterface;

class TextElement implements RenderableInterface
{
private $text;

public function __construct(string $text)
{
    $this->text = $text;
}
public function render(): string
{
    return $this->text;
}
}