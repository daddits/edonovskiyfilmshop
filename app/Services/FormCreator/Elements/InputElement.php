<?php

namespace App\Services\FormCreator\Elements;

use App\Services\FormCreator\Interfaces\RenderableInterface;

class InputElement implements RenderableInterface
{
    public function render(): string
    {
        return '<input type="text">';
    }

}