<?php
/**
 * Created by PhpStorm.
 * User: daddits
 * Date: 09.11.18
 * Time: 22:25
 */

namespace App\Services\FormCreator\Elements;


use App\Services\FormCreator\Interfaces\RenderableInterface;

class Button implements RenderableInterface
{
public function render():string
{
    return '<input type="submit" value="Send Form">';
}
}