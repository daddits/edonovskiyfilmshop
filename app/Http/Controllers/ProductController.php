<?php

namespace App\Http\Controllers;
use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function create(Request $request)
    {
        $product = new Product;
        $product->name = 'Sims City';
        $product->price = 40;
        $product->save();
        $category = Category::find([2, 5]);
        $part = Product::find([1,2]);

        $product->categories()->attach($category);
       // dd($part);
        return 'Success';
    }
    public function show(Product $product)
    {
        return view('show',compact('product'));
    }
}
