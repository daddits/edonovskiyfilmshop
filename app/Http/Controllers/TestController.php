<?php

namespace App\Http\Controllers;

use App\Services\FormCreator\Elements\Button;
use App\Services\FormCreator\Elements\InputElement;
use App\Services\FormCreator\Elements\Methods;
use App\Services\FormCreator\Elements\TextElement;
use App\Services\FormCreator\Form;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test(Request $request)
    {
        $form = new Form();

        $form->addElement(new TextElement('Email'));
        $form->addElement((new InputElement()));
        $form->addElement(new TextElement('Password'));
        $form->addElement((new InputElement()));
        $form->addElement((new Button()));
        $form = $form->render();

        return view('test', compact('form',$form));
    }
    public function testForm()
    {
        return 123;
    }
}
